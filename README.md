NFDI4Ing Trainingsmaterialien zum Datenlebenszyklus (Data Life Cycle, DLC)



### [DLC Phase 1: Forschungsvorhaben planen](https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/NFDI4Ing_Training_DLC_1_Forschungsvorhaben_planen.pdf)



### [DLC Phase 2: Daten erheben](https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/NFDI4Ing_Training_DLC_2_Daten_erheben.pdf)



### [DLC Phase 4: Forschungsdaten teilen und publizieren](https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/NFDI4Ing_Training_DLC_4_Daten_teilen_und_publizieren.pdf)



### [DLC Phase 5: Forschugnsdaten archivieren](https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/NFDI4Ing_Training_DLC_5_Forschungsdaten_archivieren.pdf)



### [DLC Phase 6: Forschungsdaten nachnutzen](https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/NFDI4Ing_Training_DLC_6_Forschungsdaten_nachnutzen.pdf)



### Nachnutzung
Bei einer Nachnutzung von diesen Unterlagen geben Sie bitte, abhängig ob die Materialien verändert wurden oder nicht, die folgende Namensnennung an:

<details><summary>Die Materialien wurden nicht verändert</summary>

Für eine vollständige Namensnennung empfehlen wir Ihnen, wenn Sie Materialien von diesem GitLab nachnutzen möchten, folgendes:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Dieser Inhalt wurde von dem [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) nachgenutzt unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>. Die nachgenutzten Materialien finden sich unter: https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle

Sollten es nicht möglich sein, die vollständige Namensnennung aufzuführen, nutzen Sie bitte folgenden Link:
https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/LICENSE_Unver%C3%A4ndert

</details>

<details><summary>Die Materialien wurden verändert</summary>

Für eine vollständige Namensnennung empfehlen wir Ihnen, wenn Sie Materialien von diesem GitLab nachnutzen möchten, folgendes:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Dieser Inhalt wurde von dem [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) nachgenutzt und verändert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>. Die nachgenutzten Materialien finden sich unter: https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle

Sollten es nicht möglich sein, die vollständige Namensnennung aufzuführen, nutzen Sie bitte folgenden Link:
https://git.rwth-aachen.de/nfdi4ing/education/data-life-cycle/-/blob/main/LICENSE_Ver%C3%A4ndert

</details>

Weitere Infos zu Lizenzen und der Nachnutzung der hier bereitgestellten Materialien finden Sie [hier](https://git.rwth-aachen.de/groups/nfdi4ing/education/-/wikis/Lizenzen).

### Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Diese Seite und ihre Inhalte sind lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.
